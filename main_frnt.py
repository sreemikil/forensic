import streamlit as st
import sqlite3
import base64
import pandas as pd
import numpy as np
from keras.models import model_from_json
from keras import backend as K
import matplotlib.pyplot as plt
from scipy.ndimage.filters import uniform_filter1d, gaussian_filter
# pip install git+https://www.github.com/keras-team/keras-contrib.git
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from keras.models import load_model
import matplotlib.pyplot as plt
import numpy as np
import cv2
from PIL import Image
conn = sqlite3.connect('data3.db')
c= conn.cursor() 

def create_table():    
    c.execute('CREATE TABLE IF NOT EXISTS user(username TEXT UNIQUE,password TEXT NOT NULL)')
    
def login_user(username,password):
    c.execute('SELECT * FROM user WHERE username =? AND password = ?',(username,password))
    data = c.fetchall() 
    return data 

def insert(name,password):
    conn=sqlite3.connect('data3.db')
    cur = conn.cursor() 
    if cur.execute('SELECT * FROM user WHERE username = ?', (name,)):
        if cur.fetchone(): 
          st.warning('Error User already exist')
        else:
           cur.execute('INSERT INTO user VALUES(?,?)', (name, password))
           st.success('Register Entry sucess')
    conn.commit()
    conn.close() 

def view_all_users():
    c.execute('SELECT * FROM user')
    data = c.fetchall()
    return data 

def generate_login_block():
    block1 = st.empty() 
    block2 = st.empty() 
    return block1, block2 

def clean_blocks(blocks):
    for block in blocks:
        block.empty()

def login1(blocks):
    blocks[0].markdown("""
        <style>
                input {
                -webkit-text-security: disc;
            }
        </style>
        """, unsafe_allow_html=True)

    return blocks[0].text_input('username')

def login2(blocks):
    blocks[1].markdown("""
        <style>
                input {
                -webkit-text-security: disc; 
            }
        </style> 
        """, unsafe_allow_html=True)
    
    return blocks[1].text_input('password',type='password')

def main():
    st.set_page_config(page_title="Forensic helper",
                initial_sidebar_state="collapsed",
                page_icon="🔮")
    
    menu = ["Home","create sketch","generate photo","Login","SignUp","About"]
    choice = st.sidebar.selectbox("Menu",menu)
    #if choice == "users":
     #   c = view_all_users()
      #  st.write(c)    
    if choice == "create sketch":
        html_temp = """ 
            <div> 
            <h1 style ="color:black;
                        text-align:center;
                        font-weight:bold;
                        font-size:40px;
                        font-style:normal;
                        font-family:Courier New;">Draw sketch using canvas</h1>
                        <div id='buttons' class="button1" >
            <a href="https://canvastoolkit.vercel.app/" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary navbar-btn btn-shadow btn-gradient">Draw</a>
        </div> 
                         </div>
            """
            
        st.markdown(html_temp, unsafe_allow_html=True)
    
    if choice == "Home":
        #main_bg = "11.gif"
        #main_bg_ext = "gif"

        #st.markdown(
        #    f"""
        #<style>
        #.reportview-container {{
        #    background: url(data:image/{main_bg_ext};base64,{base64.b64encode(open(main_bg, "rb").read()).decode()});
        #    background-position: center;
        #    background-repeat: no-repeat;
        #    background-size: cover;
        #}}
        #</style>
        #""",
        #    unsafe_allow_html=True)

        st.text(" ")
        st.text(" ")
        st.text(" ")
        st.text(" ")         
        st.text(" ")
        st.text(" ")
        st.text(" ")
        st.text(" ")
        html_temp = """ 
            <div> 
            <h1 style ="color:#FF00FF;
                        text-align:center;
                        font-weight:bold;
                        font-size:85px;
                        font-style:normal;
                        font-family:"Times New Roman", Times, serif;">FORENSIC ASSISTANCE SOFTWARE</h1>   </div>
            """
            
        st.markdown(html_temp, unsafe_allow_html=True) 
        
    elif choice == "Login":
        main_bg = "012.jpg"
        main_bg_ext = "jpg"

        st.markdown(
            f"""
        <style>
        .reportview-container {{
            background: url(data:image/{main_bg_ext};base64,{base64.b64encode(open(main_bg, "rb").read()).decode()});
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;

        }}

        </style>
        """,
            unsafe_allow_html=True
        )

        st.header("Login section")
    
        blk1 = generate_login_block()
        blk2 = generate_login_block()
                
        username = login1(blk1)         
        password = login2(blk2)
        c1, c2, c3 = st.beta_columns([1,1,1])
        login_check_box = c2.checkbox("Login")
        if login_check_box:
            
            result = login_user(username,password)
            if result:
                st.success("Successfully logged in as {}".format(username))
                
                list_of_pages = ['Photorealistic face generation','Reconstruction','Face matching']
                page= st.radio("TASKS",list_of_pages)
                if page=='Photorealistic face generation':
                    html_temp = """ 
                        <div> 
                        <h1 style ="color:black;
                                    text-align:center;
                                    font-weight:bold;
                                    font-size:50px;
                                    font-style:normal;
                                    font-family:"Lucida Console", "Courier New", monospace;">photo generation</h1>  
                        </div> 
                        """
                    st.markdown(html_temp, unsafe_allow_html=True)
                
                    data = st.file_uploader(
                        label="Enter the data to be tested", type=['jpg'])

                    image = Image.open('sketchtest.jpg')

                    st.image(image, caption='Default example Sketch to be tested')

                    if image is not None:
                        g_model = load_model('g_model.h5',custom_objects={'InstanceNormalization':InstanceNormalization})
                        img = load_img('sketchtest.jpg', target_size=(256, 256))
                        img = img_to_array(img)
                        dim = (256, 256)
  
                        # resize image
                        img = cv2.resize(img, dim, interpolation = cv2.INTER_NEAREST)
                        st.write(type(img))
                        # convert to numpy array
                        img = img_to_array(img)
                        st.write(img.shape)
                        
                        norm_img = (img.copy() - 127.5) / 127.5

                        g_img = g_model.predict(np.expand_dims(norm_img, 0))[0]
                        g_img = g_img * 127.5 + 127.5

                        
                        cv2.imwrite('temporary.jpg', g_img)
                        st.image('temporary.jpg')
                        #st.image(g_img)
                        
                        
            else:
                st.warning("Incorrect Username/password")
    elif choice == "SignUp":
        main_bg = "012.jpg"
        main_bg_ext = "jpg"

        st.markdown(
            f"""
            <style>
            .reportview-container {{
            background: url(data:image/{main_bg_ext};base64,{base64.b64encode(open(main_bg, "rb").read()).decode()});
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }}

        </style>
        """,
            unsafe_allow_html=True
        )
        
        html = """
        <div>
        <h1 style="color:black;text-align:center">Sign Up here</h1>
        </div>
        """
        st.markdown(html,unsafe_allow_html=True)
        new_username = st.text_input("CREATE A USER NAME","")
        new_password = st.text_input("CREATE A PASSWORD","")

        if st.button("SignUp"):
            
            create_table()
            all_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

            all_digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            total_digits = 0
            total_letters = 0
            for s in new_password:
                if s in all_digits:
                    total_digits+=1
                elif s in all_letters:
                    total_letters += 1
            if new_password==' ' or len(new_password)<5 or total_digits<3 or total_letters<4:
                st.warning("Please provide a valid password of length atleast 3 alphabets and 2 digits ")
            else:
                insert(new_username,new_password) 
                
    
    elif choice =="About":
        html_temp = """ 
        <div style ="background-color:navy;padding:13px"> 
        <h1 style ="color:white;text-align:center;">About</h1> 
        </div> 
                """
        st.markdown(html_temp, unsafe_allow_html=True)
        main_bg = "012.jpg"
        main_bg_ext = "jpg"

        st.markdown(
            f"""
        <style>
        .reportview-container {{
            background: url(data:image/{main_bg_ext};base64,{base64.b64encode(open(main_bg, "rb").read()).decode()});
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }}

        </style>
        """,
            unsafe_allow_html=True
        )
        col1, col2 = st.beta_columns(2)           
        with col1:
            html_temp = """ 
                    <div style="border:5px black;background-color: #A9A9A9;border-style: outset;"> 
                    <h3 style ="color:black;text-align:center">Authors:</h3> 
                    </div> 
                    """
            st.markdown(html_temp, unsafe_allow_html=True)
            st.text(" ")
            st.text(" ")                
            st.markdown(
            """ **[Cicy K Agnes](https://www.linkedin.com/in/cicykagnes/)**""")
            st.markdown(
            """ **[Akthar Naveed](https://www.linkedin.com/in/akthar-naveed-v-921039201)**""")
            
        with col2:
            html_temp = """ 
                    <div style="border:5px black;background-color: #A9A9A9;border-style: outset;"> 
                    <u>
                    <h3 style ="color:black;text-align:center">Dates and Source Code:</h3> </u>
                    </div> 
                    """
            st.markdown(html_temp, unsafe_allow_html=True)
            st.text(" ")
            st.text(" ")

            st.markdown(
                """Click here for the source code : **[Source code](https://github.com/cicykagnes/forensic-tool-kit)**""")

            html_temp = """ 
                    <div> 
                    <h3 style ="color:black;">Created on: 08/05/2022</h3> 
                    </div> 
                    """
            st.markdown(html_temp, unsafe_allow_html=True)
            html_temp = """ 
                    <div> 
                    <h3 style ="color:black;">Last updated: 08/06/2022</h3> 
                    </div> 
                    """
            st.markdown(html_temp, unsafe_allow_html=True)
            #if st.button('db'):
             #   d=view_all_users()
              #  st.write(d)

if __name__=='__main__':
    
    main()
